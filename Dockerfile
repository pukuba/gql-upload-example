FROM node:14

RUN mkdir -p /server

WORKDIR /server

ADD ./ /server

RUN npm install yarn; \
    yarn install;\
    yarn run build

EXPOSE 4000

CMD [ "yarn", "run", "on" ]