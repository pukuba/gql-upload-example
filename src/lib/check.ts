const validImageExtensions = [".jpg", ".jpeg", ".png"]

export const isValidImage = (fileName: String) => {
    for (const extension of validImageExtensions) {
        if (fileName.endsWith(extension) === true) {
            return true
        }
    }
    return false
}