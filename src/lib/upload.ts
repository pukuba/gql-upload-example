import AWS from "aws-sdk"
import { ReadStream, WriteStream } from "fs-capacitor"
import { createWriteStream } from "fs"
import env from "config/env"
import fs from "fs"

const endpoint = new AWS.Endpoint("https://kr.object.ncloudstorage.com")
const region = "kr-standard"

const S3 = new AWS.S3({
    endpoint,
    region,
    credentials: {
        accessKeyId: env.ID,
        secretAccessKey: env.PW,
    }
})
const Bucket = env.BUCKET

export const s3Upload = async (path: string, Key: string) =>
    await S3.putObject({
        Bucket,
        Key,
        ACL: "public-read",
        Body: fs.createReadStream(path)
    }).promise()

export const uploadStream = (stream: ReadStream, path: string) =>
    new Promise((resolve, reject) => {
        const capacitor = new WriteStream()
        const destination = createWriteStream(path)
        stream.pipe(capacitor)
        capacitor
            .createReadStream()
            .pipe(destination)
            .on('error', reject)
            .on('finish', resolve)
    })