const config = {
    DB_HOST: process.env.DB_HOST || "mongodb://mongodb:27017/test",
    PORT: process.env.PORT || 4000,
    ID: process.env.ID || "test",
    PW: process.env.PW || "test",
    BUCKET: process.env.BUCKET || "test"
}

export default config