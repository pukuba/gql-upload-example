import { uploadStream, isValidImage, s3Upload } from "lib"
import { Db } from "mongodb"
import { join } from "path"
import { File } from "config/types"
import { ApolloError } from "apollo-server-express"
import sharp from "sharp"

const path = join(__dirname, "../../media")

export default {
    imgUpload: async (
        parent: void, {
            file
        }: {
            file: File
        }, {
            db
        }: {
            db: Db
        }) => {
        const img = await file
        if (isValidImage(img.filename) === false) {
            throw new ApolloError(`file extension is not valid`)
        }
        try {
            const stream = img.createReadStream()
            const originalPath = `${path}/original/${img.filename}`
            await uploadStream(stream, originalPath)
            await sharp(originalPath)
                .resize(170, 170)
                .toFile(`${path}/sharp/${img.filename}`)
            await s3Upload(`${path}/sharp/${img.filename}`, img.filename)
            return true
        }
        catch {
            return false
        }
    }
}